from django.conf.urls import patterns, url

import views

urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^photo/$', views.photo, name='photo'),
    url(r'^photos/$', views.photos, name='photos'),
)
