from django.db import models


class Photo(models.Model):
    name = models.CharField(max_length=37, unique=True)
    image = models.ImageField(upload_to='photo/%Y/%m/%d')
    date = models.DateTimeField(auto_now_add=True)
    deleted = models.BooleanField(default=False)