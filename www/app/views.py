# -*- coding: utf-8 -*-

import uuid

from base64 import b64decode

from django.shortcuts import render
from django.http import HttpResponse, Http404, JsonResponse
from django.core.files.base import ContentFile

from models import Photo


def index(request):
    return render(request, 'app/index.html')


def photo(request):
    if request.method == 'POST':
        photo = request.POST['photo']
        # читаем data url
        photo = b64decode(photo)
        # генерируем новое название файла
        name = 'R%s.%s' % (uuid.uuid4().hex, 'png')
        # добавляем запись в бд и сохраняем
        upload_photo = Photo(
            name=name,
            image=ContentFile(photo, name))
        upload_photo.save()
        return JsonResponse({'status': 200, 'path': upload_photo.image.url})
    raise Http404

def photos(request):
    photos = Photo.objects.all().order_by('-date')[:100]
    return render(request, 'app/photos.html', {'photos': photos})