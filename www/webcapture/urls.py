from django.conf.urls import patterns, include, url
from django.contrib import admin

from webcapture import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'webcapture.views.home', name='home'),
    url(r'^', include('app.urls')),

    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$',
            'django.views.static.serve', {
                'document_root': settings.MEDIA_ROOT,
            }),
        # url(r'^static/(?P<path>.*)$',
        #     'django.views.static.serve', {
        #         'document_root': settings.STATIC_ROOT,
        #     }),
    )
